package com.evtech.tradestar;

public enum EventbusAddress {

	NewPlayer, 
	
	PlayerUpdates, 
	
	ConnectPlayer, 
	
	CreateTable, 
	
	JoinTable, 
	
	ExitTable, 
	
	TableList,

	DestroyTable, 
	
	Logout, 
	
	PlayerInfo, 
	
	PublishPlayerPresence,
	
	GetOnlinePlayers,
	
	GetLeaderBoard,
	
	GlobalLeaderBoardUpdate,
	
	LevelUp,
	
	ChargePayment,
	
	GetTimeRemainingForTopup,
	
	PerformBalanceTopup,
	
	// Client side 
	OnlinePlayers,
	
	LeaderBoardUpdateJS;
}
