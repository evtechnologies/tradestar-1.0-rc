package com.evtech.tradestar;

import java.io.File;

import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.core.sockjs.EventBusBridgeHook;
import org.vertx.java.core.sockjs.SockJSServer;
import org.vertx.java.core.sockjs.SockJSSocket;
import org.vertx.java.platform.Verticle;

import com.evtech.tradestar.xp.XPPointsCalculatorVerticle;
import com.evtech.tradestar.xp.XPVerticle;

public class Main extends Verticle {

	private HttpServer server;
	private SockJSServer sockJSServer;

	public void start() {
		container.logger().info("Main.start()");

		String webRootDir = System.getProperty("WEBROOT");
		if (webRootDir == null) {
			container.logger().error("Webroot directory not given as system property. Exiting!!!");
			System.exit(-1);
		}

		container.deployWorkerVerticle(MarketUpdater.class.getName());
		container.deployWorkerVerticle(MarketRequests.class.getName());
		container.deployWorkerVerticle(Authenticator.class.getName());
		container.deployWorkerVerticle(LobbyManager.class.getName());
		container.deployWorkerVerticle(XPVerticle.class.getName());
		container.deployWorkerVerticle(LeaderBoardVerticle.class.getName());
		container.deployWorkerVerticle(PaymentVerticle.class.getName());
		container.deployWorkerVerticle(JsoupWorker.class.getName());

		container.deployVerticle(XPPointsCalculatorVerticle.class.getName());

		String ip = (String) container.env().get("OPENSHIFT_VERTX_IP");
		if (ip == null) {
			ip = "127.0.0.1";
		}
		String port = (String) container.env().get("OPENSHIFT_VERTX_PORT");
		if (port == null) {
			port = "8080";
		}

		container.logger().info(ip);

		server = vertx.createHttpServer();

		server.requestHandler(new Handler<HttpServerRequest>() {
			public void handle(HttpServerRequest req) {
				if (req.path().equals("/"))
					req.response().sendFile(webRootDir + File.separatorChar + "index.html");
				else
					req.response().sendFile(webRootDir + File.separatorChar + req.path());
			}
		});

		JsonArray permitted = new JsonArray();
		permitted.add(new JsonObject());

		// final ServerHook hook = new ServerHook(logger);

		sockJSServer = vertx.createSockJSServer(server);
		// sockJSServer.setHook(hook);
		sockJSServer.bridge(new JsonObject().putString("prefix", "/eventbus"), permitted, permitted);
		sockJSServer.setHook(new EventBusBridgeHook() {

			@Override
			public boolean handleUnregister(SockJSSocket sockJSSocket, String str) {
				container.logger().info("EventBusBrideHook.handleUnregister, headers" + sockJSSocket.headers());
				return true;
			}

			@Override
			public boolean handleSocketCreated(SockJSSocket sockJSSocket) {
				container.logger().info("EventBusBrideHook.handleSocketCreated, headers" + sockJSSocket.headers());
				return true;
			}

			@Override
			public void handleSocketClosed(SockJSSocket sockJSSocket) {
				container.logger().info("EventBusBrideHook.handleSocketClosed, headers" + sockJSSocket.headers());
			}

			@Override
			public boolean handleSendOrPub(SockJSSocket sockJSSocket, boolean arg1, JsonObject arg2, String arg3) {
				container.logger().info("EventBusBrideHook.handleSendOrPub, headers" + sockJSSocket.headers());
				return true;
			}

			@Override
			public boolean handlePreRegister(SockJSSocket sockJSSocket, String arg1) {
				container.logger().info("EventBusBrideHook.handlePreRegister, headers" + sockJSSocket.headers());
				return true;
			}

			@Override
			public void handlePostRegister(SockJSSocket sockJSSocket, String arg1) {
				container.logger().info("EventBusBrideHook.handlePostRegister, headers" + sockJSSocket.headers());
			}

			@Override
			public boolean handleAuthorise(JsonObject arg0, String arg1, Handler<AsyncResult<Boolean>> arg2) {
				container.logger().info("EventBusBrideHook.handleAuthorise, headers");
				return true;
			}
		});

		server.listen(Integer.parseInt(port), ip);

		long timerID = vertx.setPeriodic(10000, new Handler<Long>() {
			public void handle(Long timerID) {
				vertx.eventBus().publish("ping", new JsonObject().putString("status", "ok").putString("serverTime",
						"Timestamp at server: " + System.currentTimeMillis()));
			}
		});

		container.logger().info("timerID " + timerID);
	}

	@Override
	public void stop() {
		container.logger().info("Main.stop()");

		if (sockJSServer != null)
			sockJSServer.close();

		if (server != null)
			server.close();

		container.logger().info("Closing the sockJS and http server.");
	}
}
